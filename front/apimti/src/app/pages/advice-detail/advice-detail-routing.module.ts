import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdviceDetailPage } from './advice-detail.page';

const routes: Routes = [
  {
    path: '',
    component: AdviceDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdviceDetailPageRoutingModule {}
