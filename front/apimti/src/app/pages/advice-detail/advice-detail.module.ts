import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdviceDetailPageRoutingModule } from './advice-detail-routing.module';

import { AdviceDetailPage } from './advice-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdviceDetailPageRoutingModule
  ],
  declarations: [AdviceDetailPage]
})
export class AdviceDetailPageModule {}
