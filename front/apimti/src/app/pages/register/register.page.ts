import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  step = 0;

  constructor() { }

  ngOnInit() {
  }

  nextStep() {
    this.step++;

    if (this.step > 1) {
      this.step = 0;
    }
  }

}
